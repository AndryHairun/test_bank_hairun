const CONFIGS = {
  TOKEN: "token",
  URL: {
    REGISTER: "/register",
    LOGIN: "/login",
    REGISTER_IBAN_BIC: "/register_iban_bic",
    TRANSACTION: "/transactions",
  },
  ENDPOINTS: {
    BASE_URL: "http://localhost:8080",
    USERS: "/users",
    LOGIN: "/user/login",
    SEND_TRANSACTION: "/transaction",
    WALLET: "/wallet",
  },
  STYLE: {
    inputTextOrPasswordStyle:
      "appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md mb-2 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm",
  },
};

export default CONFIGS;
