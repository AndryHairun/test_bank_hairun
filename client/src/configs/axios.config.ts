export const AXIOS_CONFIGS = {
  METHOD_HTTP: {
    POST: "POST",
    GET: "GET",
    DELETE: "DELETE",
    PUT: "PUT",
  },
};
