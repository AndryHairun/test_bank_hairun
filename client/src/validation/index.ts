import * as Yup from "yup";

export const RegisterValidationSchema = Yup.object({
  lastname: Yup.string().required("Nom requis !"),
  firstname: Yup.string().required("Prénom(s) requis"),
  email: Yup.string()
    .email("Entrer un email valide !")
    .required("Email requis !"),
  adress: Yup.string().required("Adresse requis"),
  cin: Yup.string()
    .trim()
    .required("CIN requis !")
    .test("len", "Doit comporter 14 caracteres", (val) => val.length === 12),
  password: Yup.string()
    .required("Requis")
    .min(8, "Doit comporter plus de 8 caracteres")
    .matches(/[a-z]+/, "doit comporter une lettre minuscule au minimum")
    .matches(/[A-Z]+/, "doit comporter une lettre majuscule au minimum")
    .matches(/[@$!%*#?&]+/, "doit comporter un caractere special au minimum")
    .matches(/\d+/, "doit comporter une chiffre au minimum"),
});

export const LoginValidationSchema = Yup.object({
  identifiant: Yup.string()
    .email("Entrer un email valide !")
    .required("Identifiant requis !"),
  password: Yup.string().required("Mot de passe requis !"),
});
