export interface ILogin {
  identifiant: string;
  password: string;
}

export interface IRegister {
  email: string;
  firstname: string;
  lastname: string;
  cin: string;
  adress: string;
  password: string;
}

export interface IWallet {
  iban: string;
  bic: string;
  solde: number;
  user_id: string;
}
