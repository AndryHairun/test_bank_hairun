import { Dispatch, SetStateAction, createContext, useContext } from "react";

interface AuthContextInterface {
  id: string | null;
  setId: Dispatch<SetStateAction<string | null>>;
}

/**
 * AuthContext
 */
const AuthContext = createContext<AuthContextInterface>({
  id: null,
  setId: () => "",
});

const useAuthContext = () => useContext(AuthContext);

export { AuthContext as default, useAuthContext };
