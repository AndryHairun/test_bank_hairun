"use client";
import React, { useEffect, useState } from "react";

import AuthContext from "./AuthContext";
interface Auth {
  children: React.ReactNode;
}
export const AuthProvider: React.FC<Auth> = ({ children }) => {
  const [id, setId] = useState<string | null>("");

  return (
    <AuthContext.Provider
      value={{
        id,
        setId,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
