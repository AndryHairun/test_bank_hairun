import React from "react";
import { BounceLoader } from "react-spinners";

export const CustomBounceLoader = () => {
  return (
    <div style={{ position: "absolute", top: "50%", zIndex: 5 }}>
      <BounceLoader color="blue" loading={true} size={100} />
    </div>
  );
};
