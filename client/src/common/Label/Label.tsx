export const CustomLabel = ({ label }: CustomLabelProps) => {
  return (
    <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
      {label}
    </label>
  );
};

type CustomLabelProps = {
  label: string;
};
