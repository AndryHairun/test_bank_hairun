import CONFIGS from "@/configs";

export const CustomInputForm = (props: any) => {
  const { placeholder, type = "text" } = props;
  return (
    <input
      type={type}
      autoComplete="none"
      className={CONFIGS.STYLE.inputTextOrPasswordStyle}
      placeholder={placeholder}
      {...props}
    />
  );
};
