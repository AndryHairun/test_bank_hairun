export const CustomMessageError = (error: string | any) => {
  return (
    <div
      className="p-4 mb-6 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
      role="alert"
    >
      <span className="font-medium">Erreur!</span> {error}
    </div>
  );
};
