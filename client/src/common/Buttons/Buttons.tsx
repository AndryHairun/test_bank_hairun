export const CustomButtonSubmit = (props: any) => {
  const { label, type = "submit" } = props;
  return (
    <button
      type={type}
      {...props}
      className="group relative w-full flex justify-center mt-2 py-2 px-4 border border-transparent text-sm font-medium round-md text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-indigo-700 focus:outline-none focus-ring-2 focus:ring-offset-2 focus:ring-indigo-500"
    >
      {label}
    </button>
  );
};
