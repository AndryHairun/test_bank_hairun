import RegisterIbanBicComponent from "@/components/register/register_step_iban_bic";

const RegisterIbanBicRoute = () => {
  return <RegisterIbanBicComponent />;
};

export default RegisterIbanBicRoute;
