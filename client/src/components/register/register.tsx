"use client";

import React, { useState } from "react";
import Link from "next/link";
import configs from "@/configs/index";
import { useRouter } from "next/navigation";
import { useFormik } from "formik";
import type { NextPage } from "next";
import { CustomBounceLoader } from "@/common/loaderComponents/loader";
import { RegisterService } from "@/services/auth.service";
import { CustomMessageError } from "@/common/CustomMessageError/CustomMessageError";
import { CustomButtonSubmit } from "@/common/Buttons/Buttons";
import { CustomInputForm } from "@/common/Inputs/inputs";
import { useAuthContext } from "@/context/AuthContext";
import { RegisterValidationSchema } from "@/validation";
import { RegisterInputInitialValue } from "@/constants/constants";

const RegisterComponent: NextPage = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const { setId } = useAuthContext();

  const router = useRouter();
  const nextPath = (path: string) => router.push(path);

  // callback after form submission
  const handleSubmit = (values: any) => {
    try {
      setLoading(true);
      RegisterService(values).then((response: any) => {
        setId(response.data?.user?._id);
        nextPath(configs.URL.REGISTER_IBAN_BIC);
      });
    } catch (error) {
      console.log("Error", error);
    } finally {
      setLoading(false);
    }
  };

  const formik = useFormik({
    initialValues: RegisterInputInitialValue,
    onSubmit: (values) => handleSubmit(values),
    validationSchema: RegisterValidationSchema,
  });

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      {loading && <CustomBounceLoader />}
      <div className="min-h-full flex items-center justify-center px-4 sm:px-6 lg:px-8">
        <div>
          <div className="register-header">
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
              S&apos;enregistrer
            </h2>
            <p className="mt-2  text-center text-sm text-gray-600">
              Ou
              <Link
                href={configs.URL.LOGIN}
                className="font-medium text-indigo-600 hover:text-indigo-500  px-2"
              >
                Se connecter
              </Link>
            </p>
          </div>

          <form className="mt-8 space-y-6" onSubmit={formik.handleSubmit}>
            <div className="rounded-sm shadow-sm space-y-px">
              <div>
                <CustomInputForm
                  placeholder="Nom"
                  {...formik.getFieldProps("lastname")}
                />
                {formik.touched.lastname &&
                  formik.errors.lastname &&
                  CustomMessageError(formik.errors.lastname)}
              </div>
              <div>
                <CustomInputForm
                  placeholder="Prénoms"
                  {...formik.getFieldProps("firstname")}
                />
                {formik.touched.firstname &&
                  formik.errors.firstname &&
                  CustomMessageError(formik.errors.firstname)}
              </div>

              <div>
                <CustomInputForm
                  placeholder="Adresse"
                  {...formik.getFieldProps("adress")}
                />
                {formik.touched.adress &&
                  formik.errors.adress &&
                  CustomMessageError(formik.errors.adress)}
              </div>

              <div>
                <CustomInputForm
                  type="email"
                  placeholder="Email"
                  {...formik.getFieldProps("email")}
                />
                {formik.touched.email &&
                  formik.errors.email &&
                  CustomMessageError(formik.errors.email)}
              </div>

              <div>
                <CustomInputForm
                  placeholder="CIN"
                  {...formik.getFieldProps("cin")}
                />
                {formik.touched.cin &&
                  formik.errors.cin &&
                  CustomMessageError(formik.errors.cin)}
              </div>

              <div>
                <CustomInputForm
                  type="password"
                  placeholder="Mot de passe"
                  {...formik.getFieldProps("password")}
                />
                {formik.touched.password &&
                  formik.errors.password &&
                  CustomMessageError(formik.errors.password)}
              </div>
            </div>

            <div>
              <CustomButtonSubmit label="Enregistrer" />
            </div>
          </form>
        </div>
      </div>
    </main>
  );
};

export default RegisterComponent;
