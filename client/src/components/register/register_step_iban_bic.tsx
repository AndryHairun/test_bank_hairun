"use client";

import React, { useState } from "react";
import configs from "@/configs/index";
import { useRouter } from "next/navigation";
import { useFormik } from "formik";
import * as Yup from "yup";
import { CustomMessageError } from "@/common/CustomMessageError/CustomMessageError";
import { CustomButtonSubmit } from "@/common/Buttons/Buttons";
import { SendIbanBicService } from "@/services/transactions.service";
import { useAuthContext } from "@/context/AuthContext";
import { CustomBounceLoader } from "@/common/loaderComponents/loader";

const IbanBicFormInput = () => {
  const { id } = useAuthContext();
  const [loading, setLoading] = useState<boolean>(false);

  const router = useRouter();
  const nextPath = (path: string) => {
    router.push(path);
  };

  const formik = useFormik({
    initialValues: {
      iban: "",
      bic: "",
      solde: 0.0,
    },
    onSubmit: async (values: any) => {
      try {
        const { iban, bic, solde } = values;
        setLoading(true);
        console.log("id", id);

        if (iban && bic && solde) {
          await SendIbanBicService({
            ...values,
            user_id: id ?? "",
          });
          nextPath(configs.URL.TRANSACTION);
        }
      } catch (error) {
        console.log("error", error);
      } finally {
        setLoading(false);
      }
    },
    validationSchema: Yup.object({
      iban: Yup.string().required("IBAN requis !"),
      bic: Yup.string().required("BIC requis !"),
      solde: Yup.number().required("Solde requis !"),
    }),
  });
  return (
    <main className="flex min-h-screen flex-col items-center justify-start p-24">
      {loading && <CustomBounceLoader />}
      <div className="register-header">
        <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
          Enregistrement IBAN et BIC
        </h2>
      </div>

      <form
        className="iban_bic_form mt-8 space-y-6"
        onSubmit={formik.handleSubmit}
      >
        <div className="rounded-sm shadow-sm space-y-px">
          <div>
            <input
              type="text"
              autoComplete="none"
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 
                text-gray-900 rounded-t-md mb-2 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="IBAN"
              {...formik.getFieldProps("iban")}
            />
            {formik.touched.iban &&
              formik.errors.iban &&
              CustomMessageError(formik.errors.iban)}
          </div>
          <div>
            <input
              type="text"
              autoComplete="none"
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 
                text-gray-900 rounded-t-md mb-2 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="BIC"
              {...formik.getFieldProps("bic")}
            />
            {formik.touched.bic &&
              formik.errors.bic &&
              CustomMessageError(formik.errors.bic)}
          </div>
          <div>
            <input
              type="text"
              autoComplete="none"
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 
                text-gray-900 rounded-t-md mb-2 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Solde"
              {...formik.getFieldProps("solde")}
            />
            {formik.touched.solde &&
              formik.errors.solde &&
              CustomMessageError(formik.errors.solde)}
          </div>
        </div>
        <div>
          <CustomButtonSubmit label="Valider" />
        </div>
      </form>
    </main>
  );
};

export default IbanBicFormInput;
