"use client";

import React, { useState } from "react";
import Link from "next/link";
import configs from "@/configs/index";
import { useRouter } from "next/navigation";
import { useFormik } from "formik";
import type { NextPage } from "next";
import { ILogin } from "@/interfaces/index";
import { LoginService } from "@/services/auth.service";
import { CustomButtonSubmit } from "@/common/Buttons/Buttons";
import { CustomInputForm } from "@/common/Inputs/inputs";
import { LoginValidationSchema } from "@/validation";
import { LoginInputInitialValue } from "@/constants/constants";
import { CustomBounceLoader } from "@/common/loaderComponents/loader";

const LoginComponent: NextPage = () => {
  const [loading, setLoading] = useState<boolean>(false);

  const router = useRouter();
  const nextPath = (path: string) => router.push(path);
  const handleSubmit = (values: ILogin) => {
    try {
      setLoading(true);
      const { identifiant: email, password } = values;
      LoginService({ email: email, password: password }).then(() => {
        nextPath(configs.URL.TRANSACTION);
      });
    } catch (error) {
      console.error("error", error);
    } finally {
      setLoading(false);
    }
  };

  const formik = useFormik({
    initialValues: LoginInputInitialValue,
    onSubmit: (values) => handleSubmit(values),
    validationSchema: LoginValidationSchema,
  });

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      {loading && <CustomBounceLoader />}
      <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div>
          <div className="login-header">
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
              Se connecter
            </h2>
            <p className="mt-2  text-center text-sm text-gray-600">
              Ou
              <Link
                href={configs.URL.REGISTER}
                className="font-medium text-indigo-600 hover:text-indigo-500  px-2"
              >
                S&apos;enregistrer
              </Link>
            </p>
          </div>

          <form className="mt-8 space-y-6" onSubmit={formik.handleSubmit}>
            <div className="rounded-sm shadow-sm space-y-px">
              <div>
                <CustomInputForm
                  placeholder="Identifiant"
                  {...formik.getFieldProps("identifiant")}
                />
                {formik.touched.identifiant && formik.errors.identifiant && (
                  <div
                    className="p-4 mb-6 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
                    role="alert"
                  >
                    <span className="font-medium">Erreur!</span>{" "}
                    {formik.touched.identifiant}
                  </div>
                )}
              </div>

              <div>
                <CustomInputForm
                  type="password"
                  placeholder="Mot de passe"
                  {...formik.getFieldProps("password")}
                />
                {formik.touched.password && formik.errors.password && (
                  <div
                    className="p-4 mb-6 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
                    role="alert"
                  >
                    <span className="font-medium">Erreur!</span>{" "}
                    {formik.touched.password}
                  </div>
                )}
              </div>
            </div>

            <div>
              <CustomButtonSubmit label="Se connecter" />
            </div>
          </form>
        </div>
      </div>
    </main>
  );
};

export default LoginComponent;
