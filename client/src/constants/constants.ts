export const RegisterInputInitialValue = {
  email: "",
  firstname: "",
  lastname: "",
  cin: "",
  adress: "",
  password: "",
};

export const LoginInputInitialValue = {
  identifiant: "",
  password: "",
};
