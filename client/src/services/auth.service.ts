import CONFIGS from "@/configs";
import API from "./api";
import { IRegister } from "@/interfaces";
import localStorageTools from "@/utils/localStorageTools";

// Service Signin
export const LoginService = (data: any) => {
  return new Promise((resolve, reject) => {
    API.axiosPostRaw(data, `${CONFIGS.ENDPOINTS.LOGIN}`).then(
      (response: any) => {
        if (response?.data)
          localStorageTools.saveValueToLocalStorage(
            CONFIGS.TOKEN,
            response?.data?.token
          );
        resolve(response?.data);
      },
      (error: any) => {
        reject(error?.response?.data);
      }
    );
  });
};

// Service Signin
export const RegisterService = (data: IRegister) => {
  return new Promise((resolve, reject) => {
    API.axiosPostRaw(data, `${CONFIGS.ENDPOINTS.USERS}`).then(
      (response: any) => {
        if (response)
          localStorageTools.saveValueToLocalStorage(
            CONFIGS.TOKEN,
            response?.token
          );
        resolve(response);
      },
      (error: any) => {
        reject(error?.response?.data);
      }
    );
  });
};

// Get Token from locale storage
export const getTokenUser = () => {
  return localStorageTools.getValueFromLocalStorage(CONFIGS.TOKEN);
};

// Deconnexion
export const Logout = () => {
  return localStorageTools.deleteValueFromLocalStorage(CONFIGS.TOKEN);
};
