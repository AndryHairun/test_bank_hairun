import CONFIGS from "@/configs";
import localStorageTools from "@/utils/localStorageTools";
import API from "./api";
import { IWallet } from "@/interfaces";

// Service Send Wallet info Post
export const SendIbanBicService = (data: IWallet) => {
  return new Promise((resolve, reject) => {
    API.axiosPostRaw(data, `${CONFIGS.ENDPOINTS.WALLET}`).then(
      (response: any) => {
        if (response?.data)
          localStorageTools.saveValueToLocalStorage(
            CONFIGS.TOKEN,
            response?.data?.token
          );
        resolve(response?.data);
      },
      (error: any) => {
        reject(error?.response?.data);
      }
    );
  });
};
