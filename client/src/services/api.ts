import CONFIGS from "@/configs";
import axios from "axios";
import { AXIOS_CONFIGS } from "@/configs/axios.config";
import { ILogin, IRegister, IWallet } from "@/interfaces";

const axiosPostRaw = (params: ILogin | IRegister | IWallet, endpoint: string) =>
  axios({
    method: AXIOS_CONFIGS.METHOD_HTTP.POST,
    url: `${CONFIGS.ENDPOINTS.BASE_URL}${endpoint}`,
    headers: {
      "Content-Type": "application/json",
    },
    data: params,
  });

const axiosGetWithToken = (endpoint: string) =>
  axios({
    method: AXIOS_CONFIGS.METHOD_HTTP.GET,
    url: `${CONFIGS.ENDPOINTS.BASE_URL}${endpoint}`,
    headers: {
      "Content-Type": "application/json",
    },
  });

const API = {
  axiosPostRaw,
  axiosGetWithToken,
};

export default API;
