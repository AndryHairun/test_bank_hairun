module.exports = {
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
  },
  env: {
    es6: true,
  },
  extends: ["eslint:recommended", "airbnb-base", "prettier"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  plugins: ["import", "simple-import-sort", "prettier"],
  rules: {
    camelcase: "off",
    "no-underscore-dangle": "off",
    "no-return-await": "off",
    "import/no-cycle": "off",
    "import/prefer-default-export": "off",
    "no-restricted-syntax": "warn",
    "consistent-return": "off",
    "no-nested-ternary": "off",
    "prefer-destructuring": "off",
    "no-plusplus": "off",
    "simple-import-sort/imports": [
      "warn",
      {
        groups: [["^\\u0000", "^@?\\w", "^[^.]", "^\\."]],
      },
    ],
  },
  ignorePatterns: [
    ".eslintrc.js",
    "**/node_modules/*",
    "**/generated/*",
    "**/*.d.ts",
    "**/dist/*",
  ],
};
