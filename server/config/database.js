import mongoose from 'mongoose';

export const db = () =>
  mongoose.connect(
    'mongodb://bank_test:admin@mongo:27017/',
    {
      dbName: 'bank_test',
    },
  );
