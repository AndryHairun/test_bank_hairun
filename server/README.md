
## Server core banking

Serveur de gestion des traitements des operations bancaire.



## Tech Stack

**Server :** Node, Express, Mongoose

**SGBD :** MongoDB

**Container :** Docker, Docker compose

**linter :** Eslint


## Installation

Le serveur est géré par le container docker. Pour l'éxécuter, il faut se mettre sur le répértoire racine et éxécuter la commmande suivante : 

```bash
  docker-compose up --build
```
    