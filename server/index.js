import Express from 'express';
import { db } from './config/database.js';
import cors from 'cors';
import { router } from './src/routes/index.js';

db()
  .then((res) => console.log('Connexion db success'))
  .catch((err) => console.log('error connexion : ', err));

export const app = Express();
const PORT = 8080;
app.use(cors());
app.use(Express.json());
router(app);
app.listen(PORT, () =>
  console.log(`App running on http://localhost:${PORT}...`),
);
