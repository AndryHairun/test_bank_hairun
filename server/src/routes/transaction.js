import { Router } from 'express';
import { transactionController } from '../controller/transactionController.js';

const router = Router();

const {
  createTransaction,
  getAllTransaction,
  updateTransaction,
  deleteTransaction,
} = transactionController();

router.post('/', createTransaction);
router.get('/', getAllTransaction);
router.put('/', updateTransaction);
router.delete('/', deleteTransaction);

export default router;
