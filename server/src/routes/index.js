import userRouter from './user.js';
import loginRouter from './login.js';
import walletRouter from './wallet.js';
import transactionRouter from './transaction.js';

import { USERS, LOGIN, WALLET, TRANSACTION } from './constant.js';

export const router = (app) => {
  app.use(USERS, userRouter);
  app.use(LOGIN, loginRouter);
  app.use(WALLET, walletRouter);
  app.use(TRANSACTION, transactionRouter);
};
