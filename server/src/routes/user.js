import { Router } from 'express';
import { userController } from '../controller/userController.js';

const router = Router();

const { createUser } = userController();

router.post('/', createUser);

export default router;
