import { Router } from 'express';

import { loginController } from '../controller/loginController.js';

const router = Router();
const { loginUser } = loginController();

router.post('/', loginUser);

export default router;
