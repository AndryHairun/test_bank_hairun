import { Router } from 'express';
import { walletController } from '../controller/walletController.js';

const router = Router();

const { walletTransaction } = walletController();

router.post('/', walletTransaction);

export default router;
