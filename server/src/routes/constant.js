export const USERS = '/users';
export const LOGIN = '/user/login';
export const WALLET = '/wallet';
export const TRANSACTION = '/transaction';
