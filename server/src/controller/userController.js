import { User } from '../models/User.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { SECRET_TOKEN } from '../../config/config.js';

export const userController = () => {
  const createUser = async (req, res, next) => {
    try {
      if (!req.body) {
        res.status(400).json({ error: 'Invalid input' });
      }
      const { firstname, adress, email, lastname, cin, password } = req.body;
      const passwordHash = await bcrypt.hash(password, 10);
      const newUser = await User.create({
        firstname,
        adress,
        email,
        lastname,
        cin,
        password: passwordHash,
      });

      if (!newUser) {
        res.status(400).json({ error: 'User not create' });
      }
      const { _id } = newUser;
      res.status(200).json({
        token: jwt.sign({ userId: _id, email }, SECRET_TOKEN),
        user: { _id, firstname, adress, email, lastname, cin },
      });
    } catch (error) {
      next(error);
    }
  };

  return {
    createUser,
  };
};
