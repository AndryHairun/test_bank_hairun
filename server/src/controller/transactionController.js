import { Transaction } from '../models/Transaction.js';
import { Wallet } from '../models/Wallet.js';
import { EmitterTobeneficiary } from '../models/emitterToBeneficiary.js';
import { SUCCESS, IN, OUT, VIREMENT, WALLET } from '../constant.js';
import { updateWalletBalance } from '../controller/walletController.js';

export const transactionController = () => {
  const createTransaction = async (req, res, next) => {
    try {
      if (!req.body) {
        res.status(400).json({ error: 'Invalid input' });
      } else {
        const {
          bicBenef,
          ibanBenef,
          nameBenef,
          user_id_emit,
          nameEmit,
          amount,
          title,
        } = req.body;

        const walletEmit = await Wallet.findOne({ user_id: user_id_emit });
        if (walletEmit.solde < amount) {
          res.status(400).json({ error: 'Solde insuffisant' });
        } else {
          const { iban, bic } = walletEmit;
          const isExistInWallet = await Wallet.findOne({
            bic: bicBenef,
            iban: ibanBenef,
          });

          //Enregistrement bénéficiaire et émetteur
          const benefAndEmitCreate = await EmitterTobeneficiary.insertMany([
            {
              name: nameBenef,
              bic: bicBenef,
              iban: ibanBenef,
              type: 'beneficiary',
            },
            {
              name: nameEmit,
              bic: bic ?? '',
              iban: iban ?? '',
              type: 'emitter',
            },
          ]);
          //Inertion transaction
          const arrayId = [
            benefAndEmitCreate[1]._id ?? '',
            benefAndEmitCreate[0]._id,
          ];
          const newTransaction = await Transaction.create({
            type: isExistInWallet ? WALLET : VIREMENT,
            modality: isExistInWallet ? OUT : IN,
            amount,
            title,
            wallet_id: [walletEmit._id ?? '', isExistInWallet._id ?? ''],
            emitter_to_beneficiary_id: arrayId,
          });
          // update bénéficiaire et émetteur

          if (newTransaction) {
            await EmitterTobeneficiary.updateMany(
              { _id: arrayId },
              { transaction_id: newTransaction?._id ?? '' },
            );
          }

          res.status(200).json({ transaction: newTransaction });
        }
      }
    } catch (error) {
      next(error);
    }
  };

  //begin update transaction
  const updateTransaction = async (req, res, next) => {
    try {
      if (!req.body) res.status(400).json({ error: 'Invalid input' });
      else {
        const { _id, statut = 'In progress', title } = req.body;
        const oneTransaction = await Transaction.findById({ _id });
        if (oneTransaction.statut === SUCCESS && statut === SUCCESS) {
          res
            .status(400)
            .json({ error: 'The status can no longer be changed' });
        } else {
          // update transaction
          const updateTransaction = await Transaction.findByIdAndUpdate(
            { _id },
            {
              statut: statut ?? oneTransaction.statut,
              title: title ?? oneTransaction.title,
            },
          );

          if (statut && statut === SUCCESS) {
            const { amount, wallet_id } = oneTransaction;
            const emit_id = wallet_id[0] ?? '';
            const benef_id = wallet_id[1] ?? '';

            //update value wallet
            if (emit_id) {
              await updateWalletBalance(emit_id, -amount);
            }

            if (benef_id) {
              await updateWalletBalance(benef_id, amount);
            }
          }
          res.status(200).json({ transaction: updateTransaction });
        }
      }
    } catch (error) {
      next(error);
    }
  };
  //suppression transaction
  const deleteTransaction = async (req, res, next) => {
    try {
      const { _id } = req.body;
      const oneDelete = await Transaction.findByIdAndUpdate(
        { _id },
        { isDeleted: true },
      );
      res.status(200).json({ transaction: oneDelete });
    } catch (error) {
      next(error);
    }
  };

  //begin get transcation
  const getAllTransaction = async (req, res, next) => {
    try {
      const page = req?.query?.page || 1;
      const limit = req?.query?.limit || 5;
      const skip = (page - 1) * limit;
      const allTransaction = await Transaction.find({ isDeleted: false })
        .sort({ createdAt: -1 })
        .skip(skip)
        .limit(limit);
      const count = await Transaction.countDocuments({ isDeleted: false });
      const totalPages = Math.ceil(count / limit);
      res.status(200).json({
        transaction: allTransaction,
        currentPage: parseInt(page),
        totalPages,
        totalResults: count,
      });
    } catch (error) {
      next(error);
    }
  };

  return {
    createTransaction,
    getAllTransaction,
    updateTransaction,
    deleteTransaction,
  };
};
