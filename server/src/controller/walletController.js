import { Wallet } from '../models/Wallet.js';

export const walletController = () => {
  const walletTransaction = async (req, res, next) => {
    try {
      if (!req.body) {
        res.status(400).json({ error: 'Invalid input' });
      } else {
        const { bic, iban, solde, user_id } = req.body;
        const newWallet = await Wallet.create({ bic, iban, solde, user_id });
        if (!newWallet) {
          res.status(400).json({ error: 'Error create wallet' });
        } else {
          res.status(200).json({ wallet: newWallet });
        }
      }
    } catch (error) {
      next(error);
    }
  };

  return {
    walletTransaction,
  };
};

export const updateWalletBalance = async (walletId, amount) => {
  const wallet = await Wallet.findByIdAndUpdate(
    { _id: walletId },
    { $inc: { solde: amount } },
    { new: true },
  );
  return wallet;
};
