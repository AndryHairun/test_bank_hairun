import { User } from '../models/User.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { SECRET_TOKEN } from '../../config/config.js';

export const loginController = () => {
  const loginUser = async (req, res, next) => {
    try {
      if (!req.body) {
        res.status(400).json({ error: 'Invalid input' });
      } else {
        const { email: emailInput, password } = req.body;
        const userFind = await User.findOne({ email: emailInput });
        if (!userFind) {
          res.status(400).json({ error: 'User not found' });
        } else {
          const { password: passwordHash } = userFind;
          const isPasswordTrue = await bcrypt.compare(password, passwordHash);
          if (!isPasswordTrue)
            res.status(400).json({ error: 'Invalid passwprd' });
          else {
            const { _id, firstname, adress, email, lastname, cin } = userFind;
            res.status(200).json({
              token: jwt.sign({ userId: _id, email }, SECRET_TOKEN),
              user: { _id, firstname, adress, email, lastname, cin },
            });
          }
        }
      }
    } catch (error) {
      next(error);
    }
  };

  return {
    loginUser,
  };
};
