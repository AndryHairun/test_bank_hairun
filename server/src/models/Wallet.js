import mongoose, { model, Schema } from 'mongoose';

const WalletSchema = new Schema({
  bic: { type: String, required: true, unique: true },
  iban: { type: String, required: true, unique: true },
  solde: { type: Number, required: true, unique: false },
  user_id: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: true,
    unique: true,
  },
  transaction_id: [{ type: mongoose.Types.ObjectId, ref: 'Transaction' }],
});

export const Wallet = model('Wallet', WalletSchema);
