import mongoose, { model, Schema } from 'mongoose';

const UserSchema = new Schema({
  firstname: { type: String, required: true },
  adress: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  lastname: { type: String, required: true },
  password: { type: String, required: true },
  cin: { type: Number, required: true, unique: true },
  wallet_id: { type: mongoose.Types.ObjectId, ref: 'Wallet' },
});

export const User = model('User', UserSchema);
