import mongoose, { model, Schema } from 'mongoose';

const EmitterTobeneficiarySchema = new Schema({
  bic: { type: String, required: true },
  iban: { type: String, required: true },
  name: { type: String, required: true },
  type: { type: String, required: true, enum: ['emitter', 'beneficiary'] },
  transaction_id: { type: mongoose.Types.ObjectId, ref: 'Transaction' },
});

export const EmitterTobeneficiary = model(
  'EmitterTobeneficiary',
  EmitterTobeneficiarySchema,
);
