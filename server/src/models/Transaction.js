import mongoose, { model, Schema } from 'mongoose';

const TransactionSchema = new Schema({
  type: {
    type: String,
    required: true,
    enum: ['wallet_to_wallet', 'virement'],
    default: 'wallet_to_wallet',
  },
  modality: {
    type: String,
    required: true,
    enum: ['In', 'Out'],
    default: 'In',
  },
  statut: {
    type: String,
    required: true,
    enum: ['In progress', 'Pending', 'Success'],
    default: 'In progress',
  },
  amount: { type: Number, required: true },
  title: { type: String, required: true },
  wallet_id: [{ type: mongoose.Types.ObjectId, ref: 'Wallet' }],
  emitter_to_beneficiary_id: [
    { type: mongoose.Types.ObjectId, ref: 'EmitterTobeneficiary' },
  ],
  isDeleted: { type: Boolean, required: true, default: false },
});

export const Transaction = model('Transaction', TransactionSchema);
